import React, {Component} from 'react';
import { StyleSheet, Text, View, Image, Button } from 'react-native';

export default class Biodata extends Component{
    render(){
        return(
            <View style = {styles.container}>
                <View style={{marginTop:64, alignItems:'center'}}>
                    <View style = {styles.avatarcontainer}>
                        <Image style={styles.avatar} source = {require('./assets/Ewidiya.jpg')} />
                    </View>
                    <Text style={styles.name}>Eti Widiyanti</Text>
                </View>
                <View>
                </View>
                <View style={styles.status}>
                    <Text style={styles.study}>Program study : </Text>
                        <Text style ={styles.state}>Teknik Informatika</Text>
                </View>
                <View style={styles.class}>
                    <Text style={styles.kelas}>kelas : </Text>
                    <Text style ={styles.state}>Pagi C</Text>
                </View>
                <View style={styles.instagram}>
                    <Text style={styles.ig}>Instagram :</Text>
                    <Text style ={styles.state}>ewidiya_</Text>
                </View>
                <View style={styles.facebook}>
                    <Text style={styles.fb}>Facebook :</Text>
                    <Text style ={styles.state}>Etty Widiyanti</Text>
                </View>
            </View>
        )
    }
} 
const styles = StyleSheet.create({
    container:{
        backgroundColor: 'magenta',
        flex:1,
        width: 500
    },
    avatarcontainer: {
        shadowColor:'#151734',
        shadowRadius: 15,
        shadowOpacity: 0.4
    },
    avatar: {
        width: 100,
        height: 100,
    },
    name: {
        color: 'black',
        marginTop: 15,
        fontSize: 20,
        fontWeight: 'bold'
    },
    statsContainer: {
        flexDirection: "row",
        justifyContent: 'space-between',
        margin: 50,
        bottom: 20
    },
    stat: {
        alignItems: 'center',
        flex : 1
    },
    statAmount:{
        color:'#4F566D',
        fontSize: 18,
        fontWeight: 'bold'
    },
    statTitle: {
        fontSize: 16,
        color: 'black',
        fontWeight: '300',
        marginTop: 4
    },
    status: {
        bottom: -20,
        left: 100
    },
    study:{
        justifyContent: 'flex-end',
        color: 'black',
        fontSize: 15,
        fontWeight: 'bold',
    },
    class: {
        bottom:-20,
        left: 100
    },
    kelas:{
        fontWeight: 'bold',
        fontSize : 15
    },
    instagram: {
        bottom : -20,
        left : 100
    },
    ig: {
        fontSize: 15,
        fontWeight: 'bold'
    },
    facebook:{
        bottom: -20,
        left : 100
    },
    fb: {
        fontWeight :'bold',
        fontSize: 15
    },
    state : {
        fontSize : 15,
        left : 100,
        bottom :-20
    }
});