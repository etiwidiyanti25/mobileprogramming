import React, {Component} from 'react';
import { StyleSheet, Text, View, StatusBar } from 'react-native';

import Login from './login';
import Biodata from './Biodata';
import Hitung from './Hitungbalok';
export default class App extends Component {
  render(){
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor='#1c313a'
          barstyle='light-content'
          />
          <Hitung />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'grey',
    alignItems: 'center',
    justifyContent: 'center'
  }
});