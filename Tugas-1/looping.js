console.log('No 1');
console.log('LOOPING PERTAMA');
var teks = 1;
while(teks <= 20){
    {
        if(teks % 2 == 0)
        {
            console.log(teks + ' - I love coding');
        }
        teks++;
    }
}

console.log('\nLOOPING KEDUA');
var teks = 20;
while (teks >= 1){
    {
        if(teks % 2 == 0)
        {
            console.log(teks + ' - I will become a mobile developer');
        }
        teks--;
    }
}

console.log('\nNo 2');
for(var nomor = 1; nomor <= 20; nomor ++){
    if ( nomor % 2 == 1 && nomor % 3 == 0 ){
        console.log(nomor + ' - I love coding')
    } else if (nomor % 2 == 1){
        console.log(nomor + ' - Teknik')
    } else {
        console.log(nomor + ' - Informatika')
    }
}
console.log('')

console.log('\nNo 3');
class Persegi {
    run(n,m) {
        for(var i = 1; i <= n; i++){
                var x = ' ';
                for(var j = 1; j <= m; j++){
                    var x = x + '#';
            }
        console.log(x);
        }
    }
}
var persegi = new Persegi;
persegi.run(4,8);
console.log('')

console.log('\nNo 4');
function segitiga1(panjang) {
    let hasil = '';
    for ( let i = 0; i < 7; i++) {
        for (let j = 0; j <= i; j++) {
            hasil += '#';
        }
        hasil += '\n';
    }
    return hasil;
}
console.log(segitiga1(10));

console.log('\nNo 5');
class Catur {
    run(n,m) {
        for(var i = 1; i <= n; i++) {
            if (i % 2 == 0) {
                var x = '';
            } else {
                var x = ' ';
            }
            for (var j = 1; j <= m; j++) {
                if (j % 2 == 0) {
                    var x = x + '#';
                }
            }
            console.log(x);
        }
    }
}
var catur = new Catur;
catur.run(8,8);