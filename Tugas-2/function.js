//NO. 1
function teriak(){
    console.log("Halo Eti Cantik!");
}

console.log(teriak());
console.log('')

//No. 2
function kalikan(num1, num2){
    return num1 * num2;
}
var num1 = 12;
var num2 = 4;

var hasilkali = kalikan(num1, num2);
console.log(hasilkali);
console.log('')

//No. 3
function introduce(name, age, address, hobby){
    console.log("Nama saya " + name + ", umur saya " + age + ", alamat saya " + address + ", dan saya punya hobby yaitu " + hobby);
}
var name = "Agus";
var age = "30";
var address = "Jln. Malioboro, Yogyakarta";
var hobby = "Gaming";

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);